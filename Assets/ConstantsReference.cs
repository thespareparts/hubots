using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ConstantsRef
{
	public static class Tags
	{
		public static string Untagged = "Untagged";
		public static string Respawn = "Respawn";
		public static string Finish = "Finish";
		public static string EditorOnly = "EditorOnly";
		public static string MainCamera = "MainCamera";
		public static string Player = "Player";
		public static string GameController = "GameController";
	}

	public static class Layers
	{
		public static int Default = 0;
		public static int TransparentFX = 1;
		public static int IgnoreRaycast = 2;
		public static int Water = 4;
		public static int UI = 5;
		public static int PfxVolumes = 8;
		public static int bots = 9;
		public static int ground = 10;
		public static int airplane = 11;
		public static int tracker = 12;
		public static int box = 13;
		public static int GlobalProjectorLayer = 14;
		public static int DataReceivers = 15;
		public static int dropoff = 16;
		public static int pickup = 17;

		public static class ByName
		{
			public static string Default = "Default";
			public static string TransparentFX = "TransparentFX";
			public static string IgnoreRaycast = "Ignore Raycast";
			public static string Water = "Water";
			public static string UI = "UI";
			public static string PfxVolumes = "PfxVolumes";
			public static string bots = "bots";
			public static string ground = "ground";
			public static string airplane = "airplane";
			public static string tracker = "tracker";
			public static string box = "box";
			public static string GlobalProjectorLayer = "GlobalProjectorLayer";
			public static string DataReceivers = "DataReceivers";
			public static string dropoff = "dropoff";
			public static string pickup = "pickup";
		}

		public static bool IsValueOnMask(int layer, LayerMask mask)
		{
			return (mask == (mask | (1 << layer)));
		}
	}

	public static class SortingLayers
	{
		public static SortingLayer Default = SortingLayer.layers[0];

		public static class ByValue
		{
			public static int Default = 0;
		}

		public static class ByName
		{
			public static string Default = "Default";
		}
	}

	public static class Scenes
	{
		public static class Hubots_V2
		{
			public static int index = 1;
			public static string name = "Hubots_V2";
			public static bool isLoaded = true;
			public static int rootCount = 41;
		}
	}

}