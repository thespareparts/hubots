﻿using UnityEditor;

public class WebGLBuilder
{
    static void build()
    {
        // Place all your scenes here
        string[] scenes = {"Assets/_Project/_Scenes/Tests.unity"};

        string pathToDeploy = "Build/";

        BuildPipeline.BuildPlayer(scenes, pathToDeploy, BuildTarget.WebGL, BuildOptions.None);
    }
}