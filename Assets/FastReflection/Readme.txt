
--- Fast.Reflection ---

* Documentation?
- Please read the Documentation file attached

* Examples?
- Please view the example codes and run the test scene for benchmarks and examples

* Forums thread?
- http://forum.unity3d.com/threads/released-fast-reflection-delegates-for-lightning-fast-metadata-reflection.305080/

* Contact?
- You can PM me, or email me at askvexe@gmail.com

Thank you!
-- vexe
