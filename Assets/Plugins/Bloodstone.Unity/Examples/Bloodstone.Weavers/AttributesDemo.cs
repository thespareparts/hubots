﻿using System.Collections.Generic;
using UnityEngine;

namespace Bloodstone.Examples
{
    public class AttributesDemo : MonoBehaviour
    {
        [SerializedAs("_serializeAs")]
        private string _newSerializeAs;

        [TextArea]
        [Serialize]
        private string _someString;

        [Serialize]
        public float SerializeProperty { get; private set; }
        [Serialize]
        private float _serializeField;
        public float PublicSerializeField;

        [ReadOnly]
        public float ReadOnlyProperty { get; private set; }
        [ReadOnly]
        private float _readOnlyField;
        [ReadOnly]
        public float ReadOnlyField;

        [Serialize(overrideDrawer: false)]
        public Vector2 Vector2Property { get; private set; }
        [Serialize(overrideDrawer: true)]
        private Vector2 _vector2Field;
        public Vector2 _publicVector2Field;

        [Slider(1, 2)]
        public float SliderProperty { get; set; }
        [Slider(5f, 25f)]
        private float _sliderField;
        [Slider(5, 25)]
        public float PublicSliderField;

        public List<int> SomeIntList;
        [Serialize]
        private int[] _someIntArray;

        [Serialize]
        public List<int> PropertyList { get; set; }
        [Serialize]
        private List<int> PrivPropertyList { get; set; }

        [DisplayName("Display Name Property")]
        public float DNProperty { get; private set; }
        [DisplayName("Private Display Name Field")]
        private float _DNField;
        [DisplayName("Public Display Name Field")]
        public float DNField;
    }
}