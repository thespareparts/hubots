﻿using UnityEngine;

namespace Bloodstone.Examples
{
    public class MultiSelectRequireDemo : MonoBehaviour
    {
        [Require]
        private EssentialClass _essentialClass;
    }
}