﻿using UnityEngine;

namespace Bloodstone.Examples
{
    public class RequireAttributeDemo : MonoBehaviour
    {
        [Require]
        public Rigidbody RequiredRigidbody { get; set; }
        [Require]
        private Rigidbody _requiredRigidbody;
        [Require]
        public Rigidbody PublicRequiredRigidbody;

        [Require]
        public Vector2Int _valueType;

        private void OnValidate()
        {
            Debug.Log($"Was {nameof(RequiredRigidbody)} property on {gameObject.name} collected? {RequiredRigidbody != null}");
        }
    }
}