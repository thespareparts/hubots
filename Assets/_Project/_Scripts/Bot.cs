﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using EnhancedBehaviours;

public class OnBotGetSelected : UnityEvent<Transform> { }

public class Bot : EnhancedBehaviour
{
    public static List<Bot> Bots = new List<Bot>();
    public static Dictionary<Transform, Bot> BotByTransform = new Dictionary<Transform, Bot>();

    public static UnityEvent OnStep = new UnityEvent();
    public static UnityEvent OnEngineResponse = new UnityEvent();
    public static OnBotGetSelected OnGetSelected = new OnBotGetSelected();
    public static OnBotGetSelected OnGetUnselected = new OnBotGetSelected();

    [HideInInspector]
    public Transform box_t;
    public TextMeshPro flag_text;
    public LineRenderer line_path;

    public Color custom_path_selected_color = Color.white;
    public Color custom_path_color = Color.white;

    List<Vector3> marked_points = new List<Vector3>();
    int point_index;
    int index_direction = 1;

    Vector3 dest, dir;
    Vector3 prev_pos;

    public bool Moving { get; private set; }
    public bool HasBox { get; private set; }

    bool was_in_loop;
    int curr_point;
    float seconds_stucked;

    public int PointCount { get { return marked_points.Count; } }

    private static int SPEED_PARAM_HASH = Animator.StringToHash("speed");

    Vector3 initialPosition;

    private void Awake()
    {
        Initialize();

        itsNavmeshAgent.updateRotation = true;

        initialPosition = itsTransform.position;
    }

    private void OnEnable()
    {
        EventsScheduler.AfterUpdate.AddListener(OnUpdate);

        Bots.Add(this);
        BotByTransform.AddIfNotExists(itsTransform, this);

        flag_text.text = (Bots.Count).ToString();
    }

    private void OnDisable()
    {
        EventsScheduler.AfterUpdate.RemoveListener(OnUpdate);

        Bots.Remove(this);
        BotByTransform.Remove(itsTransform);
    }

    public void ResetPosition()
    {
        itsNavmeshAgent.nextPosition = initialPosition;
        itsTransform.position = initialPosition;   
    }

    public Vector3 PointAt(int index)
    {
        if (index >= 0 && index < marked_points.Count)
            return marked_points[index];

        return vector3Zero;
    }

    public void RemovePoint(int index)
    {
        if (index >= 0 && index < marked_points.Count)
        {
            marked_points.RemoveAt(index);
            if(Moving)
            {
                point_index--;

                if(point_index < 0 || point_index >= marked_points.Count)
                {
                    if (!was_in_loop)
                    {
                        index_direction *= -1;
                        point_index += index_direction;
                    }
                    else point_index = 0;
                }
            }
        }
    }

    public void InsertPoint(Vector3 point)
    {
        if (!Moving) Moving = true;
        marked_points.Add(point);
    }

    public void DoBotStepping()
    {
        OnStep.Invoke();
    }

    public void DoEngineResponse()
    {
        OnEngineResponse.Invoke();
    }

    public void Select()
    {
        OnGetSelected.Invoke(itsTransform);
    }

    public void Deselect()
    {
        OnGetUnselected.Invoke(itsTransform);
    }

    public void ResetPoints()
    {
        marked_points.Clear();
        point_index = 0;
        index_direction = 1;

        line_path.positionCount = 0;
    }

    public void Restart(bool is_loop)
    {
        if(is_loop)
        {
            if (!was_in_loop)
            {
                ResetPoints();
                was_in_loop = true;
            }
        }
        else
        {
            if(was_in_loop)
            {
                ResetPoints();
                 was_in_loop = false;
            }
        }
    }

    public void ResumeMoving()
    {
        if(marked_points.Count > 0)
            Moving = true;

        itsNavmeshAgent.updatePosition = true;
    }

    public void StopMoving()
    {
        itsNavmeshAgent.updatePosition = false;

        Moving = false;
        itsAnimator.SetFloat(SPEED_PARAM_HASH, 0.0f);
        itsNavmeshAgent.isStopped = true;
    }

    public bool GiveBox(Rigidbody box_rb)
    {
        if (box_rb == null) return false;

        var box_t = box_rb.transform;
        if (box_t == null) return false;

        HasBox = true;
        this.box_t = box_t;
        box_t.SetParent(itsTransform);

        var tp = itsTransform.position;
        tp.y += 1;
        box_t.position = tp;

        var package = Package.PackagesByRigidbody[box_rb];
        package.got_owned();

        return true;
    }

    public Transform GetBox()
    {
        if(HasBox)
        {
            HasBox = false;
            box_t.SetParent(null);

            var b = box_t;
            box_t = null;

            return b;
        }

        return null;
    }

    void OnUpdate()
    {
        // Check if its moving
        if (!Moving)
        {
            itsAnimator.SetFloat(SPEED_PARAM_HASH, 0.0f);
            if(!itsNavmeshAgent.isStopped) itsNavmeshAgent.isStopped = true;

            return;
        }

        itsAnimator.SetFloat(SPEED_PARAM_HASH, 1.0f);

        // Change bot destination
        dest = marked_points[point_index];
        dir = dest - itsTransform.position;

        var mag = (dir).magnitude;
        if (mag < 1.0f)
        {
            point_index += index_direction;
            if (point_index < 0 || point_index >= marked_points.Count)
            {
                if (!was_in_loop)
                {
                    index_direction *= -1;
                    point_index += index_direction;
                }
                else point_index = 0;

                return;
            }

            dest = marked_points[point_index];
        }

        if (itsNavmeshAgent.isStopped)
            itsNavmeshAgent.isStopped = false;

        itsNavmeshAgent.SetDestination(dest);

        prev_pos = itsTransform.position;

        line_path.positionCount = marked_points.Count;

        var bot_points = new List<Vector3>(marked_points);
        if (was_in_loop)
            bot_points.Add(bot_points[0]);

        line_path.SetPositions(bot_points.ToArray());

        if (Game_Manager.instance.selected_bots.Contains(itsTransform)) line_path.startColor = line_path.endColor = BotsLinePath.line_path_selected_color * custom_path_selected_color;
        else                                                            line_path.startColor = line_path.endColor = BotsLinePath.line_path_color * custom_path_color;

        line_path.loop = was_in_loop;
    }
}
