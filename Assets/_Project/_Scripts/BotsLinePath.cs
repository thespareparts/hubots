﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotsLinePath : MonoBehaviour
{
    public LineRenderer lineRenderer;

    public static Color line_path_selected_color = new Color(1.0f, 1.0f, 1.0f, 0.75f);
    public static Color line_path_color = new Color(1.0f, 1.0f, 1.0f, 0.1f);

    public float lineWidth = 0.10f;
    public Material lineMaterial;
    public float alphaWhenBotSelected = 0.75f;
    public float alphaWhenBotNotSelected = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        var line_path = lineRenderer;

        line_path.material = lineMaterial;
        line_path_selected_color.a = alphaWhenBotSelected;
        line_path_color.a = alphaWhenBotNotSelected;
        line_path.startWidth = lineWidth;
        line_path.endWidth = lineWidth;   
    }

#if UNITY_EDITOR
    void Update()
    {
        var line_path = lineRenderer;

        line_path.material = lineMaterial;
        line_path_selected_color.a = alphaWhenBotSelected;
        line_path_color.a = alphaWhenBotNotSelected;
        line_path.startWidth = lineWidth;
        line_path.endWidth = lineWidth;
    }
#endif
}
