﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class DroppoffPackageEventCallback : UnityEvent<Transform>
{

}

public class Dropoff_Package_Data : MonoBehaviour
{
    public DroppoffPackageEventCallback on_box_pickup = new DroppoffPackageEventCallback();

    Transform collider_transform;
    bool entered = false;

    private void LateUpdate()
    {
        if (entered)
        {
            if (Game_Manager.instance == null) return;

            var box_t = Game_Manager.instance.get_box_from_bot(collider_transform);

            if (box_t != null)
                on_box_pickup.Invoke(box_t);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        entered = true;
        collider_transform = other.transform;
    }

    private void OnTriggerExit(Collider other)
    {
        entered = false;
    }
}
