﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using Random = UnityEngine.Random;
using UnityEngine.Events;
using DG.Tweening;
public class OnSetDestinationPointEvent : UnityEvent<Vector3> { }

public class Game_Manager : MonoBehaviour
{
#if UNITY_EDITOR
    public bool sendDebugMessages = false;
#endif

    public static Game_Manager instance { get; private set; }

    public static OnSetDestinationPointEvent OnSetDestinationPoint = new OnSetDestinationPointEvent();

    public enum Game_State { MENU, GAMEPLAY, ENDGAME }
    [HideInInspector]
    public Game_State state = Game_State.MENU;

    public Camera cam;
    public Transform cam_parent;
    public GameObject globalPfxObj;

    public Storage_Slot storage_slot;

    public float bots_speed = 35.0f;

    public LayerMask interactable_objs_layer;
    public LayerMask ground_layer, bots_layer;

    [HideInInspector]
    public List<Transform> selected_bots = new List<Transform>();

    public static WaitForEndOfFrame eof = new WaitForEndOfFrame();
    bool take_screenshot = false;

    public Vector2Int minMaxBoxesToDeliver = new Vector2Int(24, 40);
    public TextMeshProUGUI points_text;
    public TextMeshPro packages_text;
    public TextMeshProUGUI game_over_title;

    public Image overlay_image;

    //public GameObject[] selected_bot_vfxs = new GameObject[3];
   // Transform[] selected_bot_vfxs_t = new Transform[3];

    int points = 0;
    int boxes_count = 0;
    int boxes_to_deliver;

    Simple_Counter points_counter = new Simple_Counter();
    float elapsed_points_time = 0.0f;

    public Transform waypoint_highlight_sphere;
    Simple_Counter point_selection_cast_timer = new Simple_Counter();
    Vector3 selected_point;
    bool is_selecting_a_point;
    int point_selected_bot_index;
    int point_selected_index;

    bool is_mouse_selecting = false;
    Vector3 mouse_pos1;

    public Toggle fullscreen_toggle;

    Vector3 vzero = Vector3.zero;
    Transform bot_t = null;
    private int speedParamHash = Animator.StringToHash("speed");

    void Awake ()
    {
        instance = this;

        points_counter.Start(0.0f, true);
        point_selection_cast_timer.Start(0.15f);

       // for (int i = 0; i < selected_bot_vfxs.Length; i++)
          //  selected_bot_vfxs_t[i] = selected_bot_vfxs[i].transform;

#if UNITY_EDITOR
        StartCoroutine(check_screenshots());
#endif
        //#if !UNITY_EDITOR

        //        if (!globalPfxObj.activeInHierarchy)
        //            globalPfxObj.SetActive(true);

        //#endif
    }

    public void reset_timer_and_score(int amnt)
    {
        points_text.text = "POINTS: " + (amnt).ToString();
    }

    void Update()
    {

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.F4))
        {
#if UNITY_EDITOR && sendDebugMessages
            Debug.Log("Taking a screenshot");
#endif

            take_screenshot = true;
        }
#endif

        if(Input.GetKeyDown(KeyCode.Escape))
            fullscreen_toggle.isOn = false;

        var dt = Time.deltaTime;

        if (state == Game_State.GAMEPLAY)
        {
            elapsed_points_time += dt;
            points_counter.Update(elapsed_points_time);

            handle_user_input();
            simulate_bots(dt);
        }
        else
        {
            is_mouse_selecting = false;
        }
    }

    void on_state_change()
    {
        switch(state)
        {
            case Game_State.MENU:
                {
                    var amountOfBotAgents = Bot.bots.Count;
                    for (int i = 0; i < amountOfBotAgents; i++)
                    {
                        bot_t = Bot.bots[i].itsTransform;

                        var ba = Bot.bots_By_Transform[bot_t];

                        ba.StopMoving();
                    }
                }
                break;

            case Game_State.GAMEPLAY:
                {
                    boxes_count = 0;

                    boxes_to_deliver = Random.Range(minMaxBoxesToDeliver.x, minMaxBoxesToDeliver.y);
                    packages_text.text = boxes_count + "/" + boxes_to_deliver;

                    var amountOfBotAgents = Bot.bots.Count;
                    for (int i = 0; i < amountOfBotAgents; i++)
                    {
                        bot_t = Bot.bots[i].itsTransform;

                        var ba = Bot.bots_By_Transform[bot_t];
                        ba.ResumeMoving();
                    }
                }
                break;

            case Game_State.ENDGAME:
                {
                    var amountOfBotAgents = Bot.Bots.Count;
                    for (int i = 0; i < amountOfBotAgents; i++)
                    {
                        bot_t = Bot.Bots[i].itsTransform;
                        var ba = Bot.BotByTransform[bot_t];

                        ba.StopMoving();
                        ba.ResetPoints();
                        ba.ResetPosition();

                        Transform returned_box = ba.GetBox();
                        if(returned_box)
                            returned_box.gameObject.SetActive(false);
                    }
                    
                    storage_slot.empty_storage_and_get_amount();

                    game_over_title.text = points.ToString();
                    game_over_title.gameObject.SetActive(true);

                    Color c = Color.black;
                    c.a = 0.0f;

                    Sequence end_sequence = DOTween.Sequence();
                    end_sequence.AppendInterval(1.5f);
                    end_sequence.Append(overlay_image.DOColor(Color.black, 1.0f));
                    end_sequence.AppendCallback(() =>
                    {
                        game_over_title.gameObject.SetActive(false);
                    });
                    end_sequence.Append(overlay_image.DOColor(c, 0.5f));
                    end_sequence.AppendCallback(() =>
                    {
                        set_state(Game_State.GAMEPLAY);
                    });

                }
                break;

            case Game_State.ENDGAME:
                {
                    var amountOfBotAgents = Bot.Bots.Count;
                    for (int i = 0; i < amountOfBotAgents; i++)
                    {
                        bot_t = Bot.Bots[i].itsTransform;
                        var ba = Bot.BotByTransform[bot_t];

                        ba.StopMoving();
                        ba.ResetPoints();
                        ba.ResetPosition();

                        Transform returned_box = ba.GetBox();
                        if(returned_box)
                            returned_box.gameObject.SetActive(false);
                    }
                    
                    storage_slot.empty_storage_and_get_amount();

                    game_over_title.text = points.ToString();
                    game_over_title.gameObject.SetActive(true);

                    Color c = Color.black;
                    c.a = 0.0f;

                    Sequence end_sequence = DOTween.Sequence();
                    end_sequence.AppendInterval(1.5f);
                    end_sequence.Append(overlay_image.DOColor(Color.black, 1.0f));
                    end_sequence.AppendCallback(() =>
                    {
                        game_over_title.gameObject.SetActive(false);
                    });
                    end_sequence.Append(overlay_image.DOColor(c, 0.5f));
                    end_sequence.AppendCallback(() =>
                    {
                        set_state(Game_State.GAMEPLAY);
                    });

                }
                break;
        }

    }

    private readonly Vector3 wayPointOffset = Vector3.down * 10f;
    private RaycastHit mouseHit;
    void handle_user_input()
    {
        var left_btn = Input.GetMouseButtonDown(0);
        var right_btn = Input.GetMouseButtonDown(1);
        var shift = Input.GetKey(KeyCode.LeftShift);
        var alt = Input.GetKey(KeyCode.LeftAlt);
        var ctrl = Input.GetKey(KeyCode.LeftControl);
        var delete = Input.GetKey(KeyCode.Delete);

        var left_btn_up = Input.GetMouseButtonUp(0);

        if (!shift)
            is_selecting_a_point = false;

        if (is_selecting_a_point)   waypoint_highlight_sphere.position = selected_point;
        else                        waypoint_highlight_sphere.position = wayPointOffset;

        if (shift && point_selection_cast_timer.Ended())
        {
            is_selecting_a_point = false;

            var ray = cam.ScreenPointToRay(Input.mousePosition + wayPointOffset);
            if (Physics.Raycast(ray, out mouseHit, 1000, interactable_objs_layer))
            {
                var obj_t = mouseHit.transform;
                var layer = obj_t.gameObject.layer;
                if (!Game_Utils.layer_is(layer, bots_layer))
                {
                    var p = mouseHit.point;
                    var selected_bots_count = selected_bots.Count;
                    for (int i = 0; i < selected_bots_count; i++)
                    {
                        var bot_d = selected_bots[i];
                        var ba = Bot.bots_By_Transform[bot_d];

                        p.y = bot_d.position.y;

                        var nearest_index = 0;
                        var nearest_mag = 1000000000.0f;
                        for (int mp = 0; mp < ba.PointCount; mp++)
                        {
                            var marked_point = ba.PointAt(mp);
                            var mag = (marked_point - p).magnitude;
                            if (mag < nearest_mag)
                            {
                                nearest_mag = mag;
                                nearest_index = mp;
                            }
                        }

                        if (nearest_mag <= 0.6f)
                        {
                            selected_point = ba.PointAt(nearest_index);
                            is_selecting_a_point = true;

                            point_selected_index = nearest_index;
                            point_selected_bot_index = i;
                        }
                    }
                }
            }

            point_selection_cast_timer.Reset();
        }

        // Delete a waypoint
        if(is_selecting_a_point && left_btn)
        {
            var bot_d = selected_bots[point_selected_bot_index];
            var ba = Bot.bots_By_Transform[bot_d];

            if (ba.PointCount > 0) ba.RemovePoint(point_selected_index);

            return;
        }

        // If attempts to crate a path with right button while selecting a waypoint, cancel it
        if (is_selecting_a_point && right_btn) return;

        /////

        if (left_btn)
        {
            for (int i = 0; i < selected_bots.Count; i++)
                Bot.bots_By_Transform[selected_bots[i]].Deselect();

            selected_bots.Clear();

            is_mouse_selecting = true;
            mouse_pos1 = Input.mousePosition;
        }
        if (left_btn_up)
            is_mouse_selecting = false;

        if (left_btn || right_btn)
        {
            var ray = cam.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out mouseHit, 1000, interactable_objs_layer))
            {
                var obj_t = mouseHit.transform;
                var layer = obj_t.gameObject.layer;
                if (Game_Utils.layer_is(layer, bots_layer))
                {
                    if (left_btn)
                    {
                        for (int i = 0; i < selected_bots.Count; i++)
                            Bot.bots_By_Transform[selected_bots[i]].Deselect();

                        selected_bots.Clear();
                        select_bot(obj_t);
                    }
                }
                else // if its not bot, then its other interactable object, then the bot should go there
                {
                    var p = mouseHit.point;
                    var selected_bots_count = selected_bots.Count;
                    if (selected_bots_count > 0)
                    {
                        for (int i = 0; i < selected_bots_count; i++)
                        {
                            var bot_d = selected_bots[i];
                            var ba = Bot.bots_By_Transform[bot_d];

                            p.y = bot_d.position.y;

                            if (right_btn)
                            {
                                if (!alt)
                                {
                                    if (!shift)
                                    {
                                        ba.ResetPoints();
                                    }
                                    else
                                    {
                                        ba.Restart(false);
                                    }
                                }
                                else
                                {                         
                                    ba.Restart(true);
                                }

                                ba.InsertPoint(p);

                                OnSetDestinationPoint.Invoke(p);
                            }

                        }
                    }
                }
            }
        }
    }

    void simulate_bots(float dt)
    {
        var amountOfBotAgents = Bot.bots.Count;
        for (int i = 0; i < amountOfBotAgents; i++)
        {
            bot_t = Bot.bots[i].itsTransform;

            if(is_within_selection_bounds(bot_t.position))
                select_bot(bot_t);

            if (Input.GetKeyDown(KeyCode.Alpha1 + i))
            {
                selected_bots.Clear();
                select_bot(bot_t);
            }
        }
    }

    void handle_cam_movement()
    {
        var sd = Input.mouseScrollDelta.y;
        var curr_ortho_size = cam.orthographicSize;
        curr_ortho_size -= sd;
        cam.orthographicSize = Mathf.Clamp(curr_ortho_size, 4, 14);

        var h = Input.GetAxisRaw("Horizontal");
        var v = Input.GetAxisRaw("Vertical");

        cam_parent.position += h * cam_parent.right + v * cam_parent.forward;
    }

    public bool is_bot_available(Transform t)
    {
        var ba = Bot.bots_By_Transform[t];
        return !ba.hasBox;
    }

    public bool give_box_to_bot(Transform t, Rigidbody box)
    {
        var ba = Bot.bots_By_Transform[t];
        return ba.GiveBox(box);
    }

    public Transform get_box_from_bot(Transform t)
    {
        var ba = Bot.bots_By_Transform[t];

        Transform returned_box = ba.GetBox();

        if (returned_box)
        {
            // increase one box added
            boxes_count++;
            packages_text.text = boxes_count + "/" + boxes_to_deliver;

            var diff = points_counter.GetOffset();

            diff = Mathf.Clamp(diff, 0, 100000000000.0f);
            points += Random.Range((int)diff, (int)(diff + diff / 2));
            points_text.text = "Points  " + points.ToString();

            points_counter.Reset();

            if(boxes_count >= boxes_to_deliver)
                set_state(Game_State.ENDGAME);

            return returned_box;
        }

        return returned_box;
    }

    void select_bot(Transform obj_t)
    {
        if (!selected_bots.Contains(obj_t) && Bot.bots_By_Transform.ContainsKey(obj_t))
        {
            selected_bots.Add(obj_t);
            Bot.bots_By_Transform[obj_t].Select();

#if UNITY_EDITOR && sendDebugMessages
            Debug.Log("Selected bot " + obj_t.gameObject);
#endif
        }
    }

    public void set_state(Game_State state)
    {
        this.state = state;
        on_state_change();
    }

    public void toggle_simulation()
    {
        state = (state == Game_State.GAMEPLAY) ? Game_State.MENU : Game_State.GAMEPLAY;
        on_state_change();
    }

    public void toggle_volume()
    {
        AudioListener.volume = (AudioListener.volume > 0.0f) ? 0.0f : 1.0f;
    }

    public void toggle_fullscreen()
    {
        if (Screen.fullScreen)
        {
            Screen.fullScreen = false;
            Screen.SetResolution(960, 540, false);
        }
        else
        {
            Screen.fullScreen = true;
        }
    }

    public bool is_within_selection_bounds(Vector3 p)
    {
        if (!is_mouse_selecting)
            return false;

        var viewport_bounds = Game_Utils.get_viewport_bounds(cam, mouse_pos1, Input.mousePosition);
        return viewport_bounds.Contains(cam.WorldToViewportPoint(p));
    }

    void OnGUI()
    {
        if (is_mouse_selecting)
        {
            var rect = Game_Utils.get_screen_rect(mouse_pos1, Input.mousePosition);

            Game_Utils.draw_screen_rect(rect, new Color(0.8f, 0.8f, 0.95f, 0.25f));
            Game_Utils.draw_screen_rect_border(rect, 2, new Color(0.8f, 0.8f, 0.95f));
        }
    }

    private void OnDrawGizmos()
    {
        if (is_selecting_a_point)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere(selected_point, 0.3f);
        }
    }

#if UNITY_EDITOR
    IEnumerator check_screenshots()
    {
        while(Application.isPlaying)
        {
            yield return eof;
            if (take_screenshot)
            {
                ScreenCapture.CaptureScreenshot("Screenshots/screenshot_" + DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".png");
                take_screenshot = false;

                // wait for half a second to take another
                yield return new WaitForSeconds(0.5f);
            }
        }
    }
#endif
}
