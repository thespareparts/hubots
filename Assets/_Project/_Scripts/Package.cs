﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using EnhancedBehaviours;
using UnityEngine.Events;
using UltEvents;
using Bloodstone;
using Gamelogic;
using NaughtyAttributes;
using ReadOnly = Bloodstone.ReadOnlyAttribute;


public enum PackaggeState
{
	Dispatching,
	Dispatched,
	Waiting_ForPickup,
	Picked_Up
}

public class Package : EnhancedBehaviour.PoolItem<Package>
{
	[ReadOnly, SerializeField] private PackaggeState _currentPackageState = PackaggeState.Dispatched;
	public PackaggeState currentPackageState => _currentPackageState;

	public delegate void Package_Collision_Delegate (Package triggeringPackage, Collision collisionData);
	public static event Package_Collision_Delegate any_Package_Impact;
	[SerializeField] private UltEvent on_Impact;

	private static Dictionary<Rigidbody, Package> _packages_By_Rigidbody
	= new Dictionary<Rigidbody, Package>(90);
	public static Dictionary<Rigidbody, Package> packages_By_Rigidbody
	=> _packages_By_Rigidbody;

	[SerializeField] private BoxCollider _box_collider;
	public BoxCollider box_collider => _box_collider;

	[MinMaxSlider(0.5f,3f), SerializeField] private Vector2 _scale_Range;
	[ReadOnly, SerializeField] private Vector3 currentRandomScale;
	public Vector2 scale_Range => _scale_Range;

	private void Start() { Initialize(); }

	private void LateUpdate()
	{
		if (currentPackageState == PackaggeState.Dispatched)
		{
			RemovePackageFromPhysics();
			gameObject.SetActive(false);
		}
	}

	private void OnDestroy() { packages_By_Rigidbody.Remove(itsRigidbody);  }

	protected override void Initialize()
	{
		base.Initialize();
		RandomizeScale();
		packages_By_Rigidbody.AddIfNotContains(itsRigidbody, this);
	}

	private void RandomizeScale()
	{
		var randomScale = _scale_Range.RandomInRange();
		currentRandomScale.x = randomScale;
		currentRandomScale.y = randomScale;
		currentRandomScale.z = randomScale;

		itsTransform.localScale = currentRandomScale;
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (itsRigidbody.useGravity && itsRigidbody.velocity.magnitude > 1f)
		{
			if (on_Impact.HasCalls) on_Impact.InvokeSafe();
			any_Package_Impact?.Invoke(this, collision);
		}
	}

	protected override void BeforebeingSpawned()
	{
		//ReturnPackageToPhysics();
		RemovePackageFromPhysics();
		_currentPackageState = PackaggeState.Dispatching;
		itsNavmeshObstacle.enabled = true;
	}

	public void LoadedByBot()
	{
		RemovePackageFromPhysics();
		itsNavmeshObstacle.enabled = false;
	}

	public void ReturnPackageToPhysics()
	{
		box_collider.enabled = true;
		itsRigidbody.useGravity = true;
		itsRigidbody.constraints = RigidbodyConstraints.None;
		itsRigidbody.collisionDetectionMode = CollisionDetectionMode.Discrete;
		itsRigidbody.isKinematic = false;
		itsRigidbody.detectCollisions = true;
		itsRigidbody.WakeUp();
	}

	private void RemovePackageFromPhysics()
	{
		box_collider.enabled = false;
		itsRigidbody.useGravity = false;
		itsRigidbody.velocity = vector3Zero;
		itsRigidbody.angularVelocity = vector3Zero;
		itsRigidbody.constraints = RigidbodyConstraints.FreezeAll;
		itsRigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
		itsRigidbody.isKinematic = true;
		itsRigidbody.detectCollisions = false;
		itsRigidbody.Sleep();
	}
}
