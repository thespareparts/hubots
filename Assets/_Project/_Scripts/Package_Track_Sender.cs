﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using EnhancedBehaviours;

[System.Serializable]
public class OnSendNewPackage_Event : UnityEvent<Package_Track_Sender> {}
public class OnPackageReachedEnd_Event : UnityEvent<Package_Track_Sender> { }
public class OnPackageIsCollected_Event : UnityEvent<Package_Track_Sender> { }

public class Package_Track_Sender : EnhancedBehaviour
{
    public static OnSendNewPackage_Event OnSendNewPackage = new OnSendNewPackage_Event();
    public static OnPackageReachedEnd_Event OnPackageReachedEnd = new OnPackageReachedEnd_Event();
    public static OnPackageIsCollected_Event OnPackageIsCollected = new OnPackageIsCollected_Event();

    public ObjectCache dataPackageCache;
    public Transform begin;
    public Transform end;
    public float spawnChance = 66.666f;
    public Vector2 initialDelayRange = new Vector2(1, 9);
    private float startTime;
    public Vector2 spawnInterval;
    public Vector2 randomForce;
    public float throwAtDistance = 8f;
    public float packageSpeed = 15f;
    private float nextPackageSpawn;
    private List<Rigidbody> packages;
    public Color box_color;
    public Color diffuse_scattering_color;
    bool canSpawnNewPackage = true;
    public Pickup_Package_Data pickup_package;
    List<Rigidbody> packagesToRemove = new List<Rigidbody>();

    [HideInInspector]
    public float rot_y;

    void Awake()
    {
        Initialize();

        if (packages == null)
            packages = new List<Rigidbody>();        

        packages.Clear();

        startTime = Time.time + Random.Range(initialDelayRange.x, initialDelayRange.y);

        rot_y = transform.rotation.eulerAngles.y;
    }

    private void FixedUpdate()
    {
        if (Game_Manager.instance.state != Game_Manager.Game_State.GAMEPLAY) return;

        var currentTime = Time.time;
        if (currentTime < startTime) return;

        MovePackages();

        if (currentTime < nextPackageSpawn || !canSpawnNewPackage) return;
        nextPackageSpawn = currentTime + Random.Range(spawnInterval.x, spawnInterval.y);

        if (Random.Range(0f, 100f) > spawnChance) return;

        SpawnNewPackage();
    }

    private void MovePackages()
    {
        packagesToRemove.Clear();
        foreach (var package in packages)
        {
            //if (Vector3.Distance(package.transform.position, begin.position) > throwAtDistance)
            //{
            //    package.constraints = RigidbodyConstraints.None;
            //    package.AddForce((end.forward * Random.Range(randomForce.x, randomForce.y)), ForceMode.Impulse);

            //    packagesToRemove.Add(package);
            //}
            //else
            //{
            //    package.MoveByForcePushing_WithPhysics(end.forward, packageSpeed);
            //}

            package.transform.position += (end.position - begin.position).normalized * Time.deltaTime * packageSpeed;

            if(!canSpawnNewPackage && Vector3.Distance(package.transform.position, end.position) <= .15f)
            {
                pickup_package.box_input = package;
                packagesToRemove.Add(package);

                OnPackageReachedEnd.Invoke(this);
            }
        }

        foreach (var package in packagesToRemove)
        {
            packages.Remove(package);
        }
    }

    public void OnPickupPackage()
    {
        canSpawnNewPackage = true;
        OnPackageIsCollected.Invoke(this);
    }

    private void SpawnNewPackage()
    {
        var newPackage = dataPackageCache.GetFirstDisabledInstance();
        newPackage.transform.position = begin.position;
        newPackage.transform.rotation = Quaternion.identity;

        Material mat = newPackage.GetComponent<MeshRenderer>().material;
        mat.SetColor("_Color", box_color);
        mat.SetColor("_DiffuseScatteringColor", diffuse_scattering_color);

        var rb = newPackage.GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.useGravity = true;
        //rb.constraints = RigidbodyConstraints.FreezeRotation;
        rb.constraints = RigidbodyConstraints.FreezeAll;

        rb.gameObject.SetActive(true);

        packages.Add(rb);

        OnSendNewPackage.Invoke(this);

        canSpawnNewPackage = false;
    }
}
