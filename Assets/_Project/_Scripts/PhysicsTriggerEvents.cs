﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PhysicsTriggerEventCallback : UnityEvent<Collider>
{

}

public class PhysicsTriggerEvents : MonoBehaviour 
{
	public UnityEvent _OnTriggerEnter;
	public UnityEvent _OnTriggerStay;
	public UnityEvent _OnTriggerExit;

	[Space(10)]

	public PhysicsTriggerEventCallback _OnTriggerEnter1;
	public PhysicsTriggerEventCallback _OnTriggerStay1;
	public PhysicsTriggerEventCallback _OnTriggerExit1;

	void OnTriggerEnter(Collider collider)
	{
		Debug.Log(collider.gameObject.name);

		if(_OnTriggerEnter != null)
			_OnTriggerEnter.Invoke();

		if(_OnTriggerEnter1 != null)
			_OnTriggerEnter1.Invoke(collider);
	}

	void OnTriggerStay(Collider collider)
	{
		if(_OnTriggerStay != null)
			_OnTriggerStay.Invoke();

		if(_OnTriggerStay1 != null)
			_OnTriggerStay1.Invoke(collider);
	}

	void OnTriggerExit(Collider collider)
	{
		if(_OnTriggerExit != null)
			_OnTriggerExit.Invoke();

		if(_OnTriggerExit1 != null)
			_OnTriggerExit1.Invoke(collider);
	}
}
