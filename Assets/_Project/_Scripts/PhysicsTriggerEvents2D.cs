﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PhysicsTriggerEventCallback2D : UnityEvent<Collider2D>
{
}

public class PhysicsTriggerEvents2D : MonoBehaviour 
{
	public PhysicsTriggerEventCallback2D _OnTriggerEnter;
	public PhysicsTriggerEventCallback2D _OnTriggerStay;
	public PhysicsTriggerEventCallback2D _OnTriggerExit;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(_OnTriggerEnter != null)
			_OnTriggerEnter.Invoke(collider);
	}

	void OnTriggerStay2D(Collider2D collider)
	{
		if(_OnTriggerEnter != null)
			_OnTriggerStay.Invoke(collider);
	}

	void OnTriggerExit2D(Collider2D collider)
	{
		if(_OnTriggerEnter != null)
			_OnTriggerExit.Invoke(collider);
	}
}
