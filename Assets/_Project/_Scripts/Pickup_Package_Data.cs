﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Pickup_Package_Data : MonoBehaviour
{
    public Rigidbody box_input;
    public UnityEvent on_box_pickup = new UnityEvent();

    private void OnTriggerStay(Collider other)
    {
        if (box_input == null) return;

        Transform t = other.transform;

        if(Game_Manager.instance.is_bot_available(t))
        {
            if (Game_Manager.instance.give_box_to_bot(t, box_input))
            {
                on_box_pickup.Invoke();
                box_input = null;
            }
            else 
                Debug.LogError("Cant give box to bot even though he is available");
        }
    }
}
