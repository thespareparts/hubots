﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolsLoader : MonoBehaviour
{
    public int dataPackagePoolSize = 90;

    private void Awake()
    {
        for (int i = 0; i < dataPackagePoolSize; i++)
            AsynchronousLoader.instance.EnqueueAsynchronousOperation( AsynchronousLoader.OperationType.Load, "DataPackage");
    }
}
