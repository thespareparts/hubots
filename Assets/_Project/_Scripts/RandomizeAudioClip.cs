﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeAudioClip : MonoBehaviour
{
    public AudioClip[] variants;
    public bool oneshot = true;
    private AudioSource[] itsAudioEmitters;

    private void Awake()
    {
        itsAudioEmitters = GetComponents<AudioSource>();
    }

    private void OnEnable()
    {
        foreach (var audioEmitter in itsAudioEmitters)
        {
            audioEmitter.clip = variants[Random.Range(0, variants.Length)];
        }

        if (oneshot) enabled = false;
    }
}
