﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using ConstantsRef;

public class SceneLoader : MonoBehaviour
{
    WaitForEndOfFrame eof = new WaitForEndOfFrame();

    void Start()
    {
        StartCoroutine(SceneLoadingProcess());
    }

    IEnumerator SceneLoadingProcess()
    {
        var op = SceneManager.LoadSceneAsync("Audio", LoadSceneMode.Additive);

        op = SceneManager.LoadSceneAsync("Hubots_V2", LoadSceneMode.Additive);
        while (!op.isDone)
            yield return eof;

        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Hubots_V2"));

        op = SceneManager.LoadSceneAsync("Bots", LoadSceneMode.Additive);
        op = SceneManager.LoadSceneAsync("Vfx", LoadSceneMode.Additive);
    }
}
