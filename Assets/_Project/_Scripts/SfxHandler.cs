﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxHandler : MonoBehaviour
{
    public ObjectCache engineResponseSfxCache;
    public ObjectCache footstepSfxCache;
    public ObjectCache boxImpactCache;

    public void Awake()
    {
        Bot.OnStep.AddListener(PlayFootstepSfx);
        Bot.OnEngineResponse.AddListener(PlayEngineResponseSfx);

        Package.any_Package_Impact += OnPackageImpact;
    }


    private void OnDestroy()
    {
        Bot.OnStep.RemoveListener(PlayFootstepSfx);
        Bot.OnEngineResponse.RemoveListener(PlayEngineResponseSfx);

        Package.any_Package_Impact -= OnPackageImpact;
    }

    void OnPackageImpact(Package package, Collision impact)
    {
        var sfx_impact = boxImpactCache.GetNextInstance();
        sfx_impact.SetActive(true);
    }

    public void PlayEngineResponseSfx()
    {
        engineResponseSfxCache.GetNextInstance().SetActive(true);
    }

    public void PlayFootstepSfx()
    {
        footstepSfxCache.GetNextInstance().SetActive(true);
    }
}
