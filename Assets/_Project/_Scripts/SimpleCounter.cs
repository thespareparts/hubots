﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Simple_Counter
{
    float duration;
    float maxTime;
    float time;
    bool customTime;

    public void Start(float duration, bool customTime = false)
    {
        this.duration = duration;
        this.customTime = customTime;

        maxTime = 0.0f;
    }

    public void Update(float time)
    {
        this.time = time;
    }

    public void Reset()
    {
        maxTime = (customTime ? time : Time.time) + duration;
    }

    public bool Ended()
    {
        return (customTime ? time : Time.time) > maxTime;
    }

    public float GetMaxTime()
    {
        return maxTime;
    }

    public float GetOffset()
    {
        return (customTime ? time : Time.time) - maxTime;
    }
}
