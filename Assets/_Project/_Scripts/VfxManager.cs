﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EnhancedBehaviours;

public class VfxManager : EnhancedBehaviour
{
    public ParticleSystem incomingDataPackage;
    public Transform loopVfx;
    public ObjectCache dataDroppletsCache;
    public ObjectCache botSelectionCache;
    public ObjectCache setFocusCache;
    public ObjectCache setDestCache;
    public ObjectCache dataStoredCache;

    private Vector3 loopVfxCurrentPosition;

    private Dictionary<Transform, GameObject> botSelectionByBotTransform = new Dictionary<Transform, GameObject>();

    private void OnEnable()
    {
        EventsScheduler.BeforeUpdate.AddListener(OnUpdate);

        Storage_Slot.OnInsertPackage.AddListener(OnInsertPackage);

        Package_Track_Sender.any_Package_Spawned_By_Track += On_New_Package;
        Package_Track_Sender.any_Package_Reached_Loading_Point += OnPackageIsCollectedOrReachedEnd;
        Package_Track_Sender.any_Package_Picked_Up += OnPackageIsCollectedOrReachedEnd;

        Game_Manager.OnSetDestinationPoint.AddListener(OnSetDestinationPoint);
        Bot.OnGetSelected.AddListener(OnBotGetSelected);
        Bot.OnGetUnselected.AddListener(OnBotGetUnselected);
    }

    private void OnDisable()
    {
        EventsScheduler.BeforeUpdate.RemoveListener(OnUpdate);

        Storage_Slot.OnInsertPackage.RemoveListener(OnInsertPackage);

        Package_Track_Sender.any_Package_Spawned_By_Track -= On_New_Package;
        Package_Track_Sender.any_Package_Reached_Loading_Point -= OnPackageIsCollectedOrReachedEnd;
        Package_Track_Sender.any_Package_Picked_Up -= OnPackageIsCollectedOrReachedEnd;

        Game_Manager.OnSetDestinationPoint.RemoveListener(OnSetDestinationPoint);
        Bot.OnGetSelected.RemoveListener(OnBotGetSelected);
        Bot.OnGetUnselected.RemoveListener(OnBotGetUnselected);
    }

    private void OnUpdate()
    {
        loopVfx.position = Vector3.MoveTowards(loopVfx.position, loopVfxCurrentPosition, Time.smoothDeltaTime * 18f);

        foreach (var botSelection in botSelectionByBotTransform)
        {
            var bot_t = botSelection.Key;
            var selected_bot_vfx = botSelection.Value;
            var selected_bot_vfx_t = selected_bot_vfx.transform;

            var p = bot_t.position;
            p.y = selected_bot_vfx_t.position.y;
            selected_bot_vfx_t.position = p;

            selected_bot_vfx_t.rotation = bot_t.rotation;
        }
    }

    private void OnInsertPackage(Vector3 p)
    {
        var vfx = dataStoredCache.GetNextInstance();

        vfx.transform.position = p;
        vfx.SetActive(true);
    }

    private void OnBotGetSelected(Transform t)
    {
        loopVfx.gameObject.SetActive(false);
        loopVfx.position = t.position;
        loopVfxCurrentPosition = t.position;

        show_fx(setFocusCache, t.position);

        GameObject botSelectionVfx = null;
        if (!botSelectionByBotTransform.ContainsKey(t))
        {
            botSelectionVfx = botSelectionCache.GetNextInstance();
            botSelectionByBotTransform.Add(t, botSelectionVfx);
        }
        else
            botSelectionVfx = botSelectionByBotTransform[t];

        botSelectionVfx.SetActive(true);
    }

    private void OnBotGetUnselected(Transform t)
    {
        loopVfx.gameObject.SetActive(false);

#if UNITY_EDITOR
        Debug.Log(t.gameObject.name);
#endif

        if (botSelectionByBotTransform.ContainsKey(t))
        {
            botSelectionByBotTransform[t].SetActive(false);

#if UNITY_EDITOR
            Debug.Log("Deactivated");
#endif
        }
    }

    private void OnSetDestinationPoint(Vector3 p)
    {
        show_fx(setDestCache, p);

        loopVfx.gameObject.SetActive(true);
        loopVfxCurrentPosition = p;
    }

    private void OnPackageIsCollectedOrReachedEnd(Package_Track_Sender sender, Package package)
    {

    }

    private void OnPackageIsCollectedOrReachedEnd(Package_Track_Sender sender, Package package, Bot bot)
    {
        var dataDropplet = dataDroppletsCache.GetNextInstance();
        var t = dataDropplet.transform;

        //t.position = sender.package_Destination.position + sender.package_Destination.forward;

        var rot = sender.itsTransform.rotation;
        var ea = rot.eulerAngles;
        ea.y += 180.0f;
        rot.eulerAngles = ea;

        t.rotation = rot;

        dataDropplet.SetActive(true);
    }

    private void On_New_Package(Package_Track_Sender sender, Package newPackage)
    {
        var incoming_vfx_t = incomingDataPackage.transform;
        var q = incoming_vfx_t.rotation;
        var euler_q = q.eulerAngles;
        euler_q.y = sender.destination_Point.eulerAngles.y + 90.0f;
        q.eulerAngles = euler_q;
        incoming_vfx_t.rotation = q;

        //Vector3 p = sender.package_Arrival.position;
        //p.y = incoming_vfx_t.position.y;
        //incoming_vfx_t.position = p;

        incomingDataPackage.Emit(1);
    }

    private void show_fx(ObjectCache cache, Vector3 p)
    {
        var obj = cache.GetNextInstance();
        obj.transform.position = p;
        obj.SetActive(true);
    }
}
